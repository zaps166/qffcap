#include "Rectangle.hpp"

#include <QMouseEvent>
#include <QKeyEvent>
#include <QDebug>

#define thickness 3

Rectangle::Rectangle()
{
	setCursor( Qt::PointingHandCursor );
	setWindowFlags( Qt::X11BypassWindowManagerHint );
	setPalette( Qt::red );
}

void Rectangle::setGeo( int X, int Y, int W, int H )
{
	setGeometry( QRect( X - thickness, Y - thickness, W + thickness*2, H + thickness*2 ) );
}

void Rectangle::resizeEvent( QResizeEvent * )
{
	QRegion region;
	region += QRect( 0, 0, width(), thickness );
	region += QRect( 0, 0, thickness, height() );
	region += QRect( width()-thickness, 0, thickness, height() );
	region += QRect( 0, height()-thickness, width(), thickness );
	setMask( region );
	emit geometryChanged( x() + thickness, y() + thickness, width() - thickness*2, height() - thickness*2 );
}
void Rectangle::mouseMoveEvent( QMouseEvent *e )
{
	if ( e->buttons() & Qt::LeftButton )
	{
		int min = thickness*2+1;
		int W = e->pos().x();
		int H = e->pos().y();
		if ( W < min )
			W = min;
		if ( H < min )
			H = min;
		resize( W, H );
	}
	else if ( e->buttons() & Qt::RightButton )
	{
		move( mapToGlobal( e->pos() ) );
		emit geometryChanged( x() + thickness, y() + thickness, width() - thickness*2, height() - thickness*2 );
	}
}
