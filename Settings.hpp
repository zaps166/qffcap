#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include "ui_Settings.h"

#include <QSettings>

class Settings : public QDialog
{
    Q_OBJECT
public:
	explicit Settings( QWidget *parent = NULL );
	Ui::Settings ui;

	void loadSettings();
	void saveSettings();
private:
	QSettings *sets;
private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();

	void on_browseB_clicked();

	void on_defaultCodecB_clicked();
	void on_defaultAudioB_clicked();

	void on_videoCodecB_currentIndexChanged( const QString & );
	void on_audioCodecB_currentIndexChanged( const QString & );
};

#endif // SETTINGS_HPP
