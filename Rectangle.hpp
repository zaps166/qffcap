#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <QWidget>

class Rectangle : public QWidget
{
    Q_OBJECT
public:
	explicit Rectangle();

	void setGeo( int, int, int, int );
private:
	void resizeEvent( QResizeEvent * );
	void mouseMoveEvent( QMouseEvent * );
signals:
	void geometryChanged( int, int, int, int );
};

#endif // RECTANGLE_HPP
