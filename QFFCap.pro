#-------------------------------------------------
#
# Project created by QtCreator 2011-09-07T12:01:19
#
#-------------------------------------------------

QT += core gui

TARGET = QFFCap
TEMPLATE = app

OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = build/rcc
UI_DIR = build/uic

SOURCES   += MainWindow.cpp Settings.cpp Rectangle.cpp main.cpp
HEADERS   += MainWindow.hpp Settings.hpp Rectangle.hpp
FORMS     += MainWindow.ui  Settings.ui
RESOURCES += icons.qrc
