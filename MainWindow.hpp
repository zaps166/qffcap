#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "ui_MainWindow.h"

#include "Rectangle.hpp"

#include <QSystemTrayIcon>
#include <QProcess>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
	explicit MainWindow( QWidget *parent = NULL );
	~MainWindow();
private slots:
	void trayActivated( QSystemTrayIcon::ActivationReason );
	void ffmpegFinished( int );
	void ffmpegError( QProcess::ProcessError );

	void rectGeometryChanged( int, int, int, int );
	void changeRectGeometry();

	void on_actionSettings_triggered();
	void on_actionCapture_triggered();
	void on_browseB_clicked();
	void on_rectB_clicked();
	void on_manualDesktopAreaB_toggled( bool );
private:
	void getSettings();

	void closeEvent( QCloseEvent * );

	Ui::MainWindow ui;

	Rectangle rectangle;
	class Settings *settings;
	class QSettings *sets;

	QSystemTrayIcon tray;
	QProcess ffmpeg;
	QString ffmpegCommand, videoCodec, audioCodec, audioDriver, audioDevice, videoCodecParams, audioCodecParams;
	bool cantChangeGeometry;
};

#endif // MAINWINDOW_HPP
