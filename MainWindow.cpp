#include "MainWindow.hpp"

#include "Settings.hpp"

#include <QDesktopWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QThread>
#include <QFile>
#include <QDebug>

MainWindow::MainWindow( QWidget *parent ) : QMainWindow( parent )
{
	ui.setupUi( this );

	setWindowTitle( qApp->applicationName() + " v0.1 - Błażej Szczygieł ( http://zaps166.sf.net/ )" );

	tray.setIcon( QIcon( ":/stop.png" ) );
	ui.actionSettings->setIcon( QIcon( ":/settings.png" ) );
	ui.actionCapture->setIcon( QIcon( ":/cap.png" ) );

	connect( &tray, SIGNAL( activated( QSystemTrayIcon::ActivationReason ) ), this, SLOT( trayActivated( QSystemTrayIcon::ActivationReason ) ) );
	connect( &ffmpeg, SIGNAL( finished( int, QProcess::ExitStatus ) ), this, SLOT( ffmpegFinished( int ) ) );
	connect( &ffmpeg, SIGNAL( error( QProcess::ProcessError ) ), this, SLOT( ffmpegError( QProcess::ProcessError ) ) );
	connect( &rectangle, SIGNAL( geometryChanged( int, int, int, int ) ), this, SLOT( rectGeometryChanged( int, int, int, int ) ) );
	connect( ui.xPosB, SIGNAL( valueChanged( int ) ), this, SLOT( changeRectGeometry() ) );
	connect( ui.yPosB, SIGNAL( valueChanged( int ) ), this, SLOT( changeRectGeometry() ) );
	connect( ui.xResB, SIGNAL( valueChanged( int ) ), this, SLOT( changeRectGeometry() ) );
	connect( ui.yResB, SIGNAL( valueChanged( int ) ), this, SLOT( changeRectGeometry() ) );

	cantChangeGeometry = false;

	sets = new QSettings( qApp->applicationName() );
	qApp->setProperty( "settings", qVariantFromValue( ( void * )sets ) );

	ui.videoB->setChecked( sets->value( "MainWindow/videoChecked", true ).toBool() );
	ui.audioB->setChecked( sets->value( "MainWindow/audioChecked", true ).toBool() );
	ui.fpsB->setValue( sets->value( "MainWindow/fps", 30. ).toDouble() );
	ui.manualDesktopAreaB->setChecked( sets->value( "MainWindow/manualDesktopAreaChecked", false ).toBool() );
	ui.xPosB->setValue( sets->value( "MainWindow/X", 0 ).toInt() );
	ui.yPosB->setValue( sets->value( "MainWindow/Y", 0 ).toInt() );
	ui.xResB->setValue( sets->value( "MainWindow/W", 640 ).toInt() );
	ui.yResB->setValue( sets->value( "MainWindow/H", 480 ).toInt() );
	ui.sRateB->setValue( sets->value( "MainWindow/sRate", 48000 ).toInt() );
	ui.chnB->setValue( sets->value( "MainWindow/chn", 2 ).toInt() );
	ui.outFileE->setText( sets->value( "MainWindow/outFile", "out.mkv" ).toString() );

	show();

	settings = new Settings( this );
	if ( settings->ui.videoCodecB->count() <= 1 && settings->ui.audioCodecB->count() <= 1 )
		ui.actionSettings->trigger();
	else
	{
		getSettings();
		settings->saveSettings();
	}
}
MainWindow::~MainWindow()
{
	sets->setValue( "MainWindow/videoChecked", ui.videoB->isChecked() );
	sets->setValue( "MainWindow/audioChecked", ui.audioB->isChecked() );
	sets->setValue( "MainWindow/fps", ui.fpsB->value() );
	sets->setValue( "MainWindow/manualDesktopAreaChecked", ui.manualDesktopAreaB->isChecked() );
	sets->setValue( "MainWindow/X", ui.xPosB->value() );
	sets->setValue( "MainWindow/Y", ui.yPosB->value() );
	sets->setValue( "MainWindow/W", ui.xResB->value() );
	sets->setValue( "MainWindow/H", ui.yResB->value() );
	sets->setValue( "MainWindow/sRate", ui.sRateB->value() );
	sets->setValue( "MainWindow/chn", ui.chnB->value() );
	sets->setValue( "MainWindow/outFile", ui.outFileE->text() );

	delete sets;
}

void MainWindow::trayActivated( QSystemTrayIcon::ActivationReason reason )
{
	if ( reason == QSystemTrayIcon::Trigger && ui.actionCapture->isChecked() )
		ui.actionCapture->trigger();
}
void MainWindow::ffmpegFinished( int code )
{
	if ( ui.actionCapture->isChecked() )
		ui.actionCapture->trigger();
	ui.centralWidget->setEnabled( true );
	ui.actionCapture->setIcon( QIcon( ":/cap.png" ) );
	ffmpeg.readAllStandardOutput(); //clear buffer
	QByteArray std_err = ffmpeg.readAllStandardError();
	if ( code && code != 255 )
		QMessageBox::information( this, "Error", std_err );
}
void MainWindow::ffmpegError( QProcess::ProcessError err )
{
	if ( err == QProcess::FailedToStart )
	{
		QMessageBox::warning( this, qApp->applicationName(), "Failed to start FFMpeg!\nMake sure the \"FFMpeg command\" in settings is OK!" );
		if ( ui.actionCapture->isChecked() )
			ui.actionCapture->trigger();
	}
}

void MainWindow::rectGeometryChanged( int X, int Y, int W, int H )
{
	cantChangeGeometry = true;
	ui.xPosB->setValue( X );
	ui.yPosB->setValue( Y );
	ui.xResB->setValue( W );
	ui.yResB->setValue( H );
	cantChangeGeometry = false;
}
void MainWindow::changeRectGeometry()
{
	if ( !cantChangeGeometry )
		rectangle.setGeo( ui.xPosB->value(), ui.yPosB->value(), ui.xResB->value(), ui.yResB->value() );
}

void MainWindow::on_actionSettings_triggered()
{
	if ( settings->exec() )
		settings->saveSettings();
	else
		settings->loadSettings();
	getSettings();
}
void MainWindow::on_actionCapture_triggered()
{
	if ( ui.actionCapture->isChecked() )
	{
		if ( ffmpeg.state() != QProcess::NotRunning )
		{
			ui.actionCapture->setChecked( false );
			return;
		}

		if ( !ui.videoB->isChecked() && !ui.audioB->isChecked() )
		{
			QMessageBox::warning( this, qApp->applicationName(), "You must capture somthing!" );
			ui.actionCapture->setChecked( false );
			return;
		}

		QString filePath = ui.outFileE->text();

		if ( filePath.isEmpty() )
		{
			QMessageBox::warning( this, qApp->applicationName(), "Output file must be specified!" );
			ui.actionCapture->setChecked( false );
			return;
		}
		if ( QFile::exists( filePath ) && QMessageBox::question( this, qApp->applicationName(), "Do you want to overwrite output file?", QMessageBox::Yes, QMessageBox::No ) == QMessageBox::No )
		{
			ui.actionCapture->setChecked( false );
			return;
		}

		if ( QSystemTrayIcon::isSystemTrayAvailable() )
		{
			tray.setToolTip( qApp->applicationName() + " - stop capturing" );
			tray.show();
			move( x(), y() );
			hide();
		}
		else
			showMinimized();

		ui.actionCapture->setIcon( QIcon( ":/stop.png" ) );
		ui.centralWidget->setEnabled( false );

		int xOffset = 0, yOffset = 0, sWidth, sHeight;
		if ( ui.manualDesktopAreaB->isChecked() )
		{
			xOffset = ui.xPosB->value();
			yOffset = ui.yPosB->value();
			sWidth = ui.xResB->value();
			sHeight = ui.yResB->value();
		}
		else
		{
			sWidth = qApp->desktop()->width();
			sHeight = qApp->desktop()->height();
		}

		QStringList ffParams;
		if ( ui.audioB->isChecked() )
			ffParams << "-f" << audioDriver << "-ac" << QString::number( ui.chnB->value() ) << "-ar" << QString::number( ui.sRateB->value() ) << "-i" << audioDevice;
		if ( ui.videoB->isChecked() )
			ffParams << "-f" << "x11grab" << "-r" << QString::number( ui.fpsB->value() ) << "-s" << QString::number( sWidth ) + ":" + QString::number( sHeight ) << "-i" << ":0.0+" + QString::number( xOffset ) + "," + QString::number( yOffset );
		if ( ui.audioB->isChecked() )
			ffParams << "-acodec" << audioCodec << audioCodecParams.split( ' ', QString::SkipEmptyParts );
		if ( ui.videoB->isChecked() )
			ffParams << "-vcodec" << videoCodec << videoCodecParams.split( ' ', QString::SkipEmptyParts );
		ffParams << "-threads" << QString::number( QThread::idealThreadCount() );
		ffParams << "-y" << filePath;
		ffmpeg.start( ffmpegCommand, ffParams );
	}
	else
	{
		if ( tray.isVisible() )
		{
			tray.hide();
			showNormal();
		}
		if ( ffmpeg.state() != QProcess::NotRunning )
			ffmpeg.terminate();
		else
			ui.centralWidget->setEnabled( true );
	}
}
void MainWindow::on_browseB_clicked()
{
	QString fileP = QFileDialog::getSaveFileName( this, "Enter capture file name" );
	if ( !fileP.isEmpty() )
		ui.outFileE->setText( fileP );
}
void MainWindow::on_rectB_clicked()
{
	bool s = ui.rectB->isChecked();
	if ( s )
		changeRectGeometry();
	rectangle.setVisible( s );
}
void MainWindow::on_manualDesktopAreaB_toggled( bool b )
{
	if ( !b && ui.rectB->isChecked() )
	{
		ui.rectB->toggle();
		on_rectB_clicked();
	}
}

void MainWindow::getSettings()
{
	ffmpegCommand = settings->ui.ffmpegE->text();
	videoCodec = settings->ui.videoCodecB->currentText();
	videoCodecParams = settings->ui.videoParamsE->text();
	audioCodec = settings->ui.audioCodecB->currentText();
	audioCodecParams = settings->ui.audioParamsE->text();
	audioDriver = settings->ui.audioDrvE->text();
	audioDevice = settings->ui.audioDevE->text();

	if ( settings->ui.videoParamsE->isEnabled() )
		ui.videoB->setEnabled( true );
	else
	{
		ui.videoB->setEnabled( false );
		ui.videoB->setChecked( false );
	}

	if ( settings->ui.audioParamsE->isEnabled() && !settings->ui.audioDevE->text().isEmpty() && !settings->ui.audioDrvE->text().isEmpty() )
		ui.audioB->setEnabled( true );
	else
	{
		ui.audioB->setEnabled( false );
		ui.audioB->setChecked( false );
	}
}

void MainWindow::closeEvent( QCloseEvent * )
{
	qApp->quit();
}
