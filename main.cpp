#include <QApplication>
#include <QTextCodec>

#include "MainWindow.hpp"

int main(int argc, char *argv[])
{
	QTextCodec::setCodecForCStrings( QTextCodec::codecForName( "UTF-8" ) );
	QApplication app( argc, argv );
	app.setWindowIcon( QIcon( ":/icon.png" ) );
	app.setQuitOnLastWindowClosed( false );
	app.setApplicationName( "QFFCap" );

	MainWindow w;

	return app.exec();
}
