#include "Settings.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QFileInfo>
#include <QProcess>
#include <QDebug>

Settings::Settings( QWidget *parent ) : QDialog( parent )
{
	ui.setupUi( this );
	sets = ( QSettings * )qApp->property( "settings" ).value<void *>();
	loadSettings();
	ui.ffmpegE->setFocus();
}

void Settings::loadSettings()
{
	ui.ffmpegE->setText( sets->value( "Settings/FFMpeg", "ffmpeg" ).toString() );

	ui.defaultAudioB->click();
	ui.defaultCodecB->click();

	int idx;
	idx = ui.videoCodecB->findText( sets->value( "Settings/videoCodec" ).toString() );
	if ( idx > -1 )
		ui.videoCodecB->setCurrentIndex( idx );
	idx = ui.audioCodecB->findText( sets->value( "Settings/audioCodec" ).toString() );
	if ( idx > -1 )
		ui.audioCodecB->setCurrentIndex( idx );

	ui.videoParamsE->setText( sets->value( "Settings/videoParams", ui.videoParamsE->text() ).toString() );
	ui.audioParamsE->setText( sets->value( "Settings/audioParams", ui.audioParamsE->text() ).toString() );
	ui.audioDrvE->setText( sets->value( "Settings/audioDrv", ui.audioDrvE->text() ).toString() );
	ui.audioDevE->setText( sets->value( "Settings/audioDev", ui.audioDevE->text() ).toString() );
}
void Settings::saveSettings()
{
	sets->setValue( "Settings/FFMpeg", ui.ffmpegE->text() );
	if ( ui.videoCodecB->count() > 1 )
		sets->setValue( "Settings/videoCodec", ui.videoCodecB->currentText() );
	sets->setValue( "Settings/videoParams", ui.videoParamsE->text() );
	if ( ui.audioCodecB->count() > 1 )
		sets->setValue( "Settings/audioCodec", ui.audioCodecB->currentText() );
	sets->setValue( "Settings/audioParams", ui.audioParamsE->text() );
	sets->setValue( "Settings/audioDrv", ui.audioDrvE->text() );
	sets->setValue( "Settings/audioDev", ui.audioDevE->text() );
}

void Settings::on_buttonBox_accepted()
{
	if ( ui.ffmpegE->text().isEmpty() )
	{
		QMessageBox::warning( this, qApp->applicationName(), "FFMpeg command cannot be empty!" );
		ui.ffmpegE->setFocus();
		return;
	}
	accept();
}
void Settings::on_buttonBox_rejected()
{
	reject();
}

void Settings::on_browseB_clicked()
{
	QString fPath = QFileDialog::getOpenFileName( this, "Choose FFMpeg executable" );
	if ( QFileInfo( fPath ).isExecutable() )
		ui.ffmpegE->setText( fPath );
	else
		QMessageBox::information( this, qApp->applicationName(), "This file isn't executable!" );
}

void Settings::on_defaultCodecB_clicked()
{
	ui.videoCodecB->clear();
	ui.audioCodecB->clear();

	ui.videoCodecB->addItem( "off" );
	ui.audioCodecB->addItem( "off" );

	QProcess ffmpeg;
	ffmpeg.start( ui.ffmpegE->text(), QStringList() << "-codecs" );
	if ( ffmpeg.waitForFinished( 2500 ) )
	{
		foreach ( QString line, QString( ffmpeg.readAllStandardOutput() ).remove( '\r' ).split( '\n' ) )
		{
			if ( line.length() > 4 && line[ 2 ] == 'E' )
			{
				bool video = line[ 3 ] == 'V';
				bool audio = line[ 3 ] == 'A';
				if ( audio || video )
				{
					QString codec;
					line.remove( 0, 8 );
					int idx = line.indexOf( ' ' );
					if ( idx > -1 )
					{
						line.remove( idx, line.length() - idx );
						if ( video )
							ui.videoCodecB->addItem( line );
						else if ( audio )
							ui.audioCodecB->addItem( line );
					}
				}
			}
		}
	}

	int idx;
	idx = ui.videoCodecB->findText( "libx264" );
	if ( idx > -1 )
		ui.videoCodecB->setCurrentIndex( idx );
	idx = ui.audioCodecB->findText( "libvorbis" );
	if ( idx > -1 )
		ui.audioCodecB->setCurrentIndex( idx );
}
void Settings::on_defaultAudioB_clicked()
{
#ifdef Q_OS_LINUX
		ui.audioDrvE->setText( "alsa" );
		ui.audioDevE->setText( "default" );
#endif
}

void Settings::on_videoCodecB_currentIndexChanged( const QString &text )
{
	ui.videoParamsE->setEnabled( text != "off" );
	if ( text == "libx264" )
		ui.videoParamsE->setText( "-vpre lossless_ultrafast" );
	else
		ui.videoParamsE->clear();
}
void Settings::on_audioCodecB_currentIndexChanged( const QString &text )
{
	ui.audioParamsE->setEnabled( text != "off" );
	if ( text == "libvorbis" )
		ui.audioParamsE->setText( "-aq 2" );
	else
		ui.audioParamsE->clear();
}
